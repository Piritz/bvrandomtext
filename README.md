# README #

This project will generate N calls to randomtext.me api, gathering info from the fetched random text, obtaining the
most used words.

### How do I get set up? ###

Two main endpoints
	-/betvictor/text?p_start=1&p_end=100&w_count_min=1&w_count_max=25
	this endpoint will make a call to randomtext.me N times (with N = p_end - p_start), asking for a number i of 
	paragraphs (with p_start <= i <= p_end), with each paragraph containing a number of words between w_count_min and 
	w_count_max. This endpoint will keep an statistic of the most repeated words from the service
	-/betvictor/history
	this endpoint will respond with the last 10 computation results

Default configuration port will be 8088. So if running locally you should do the next request:

localhost:8088/betvictor/text?p_start=1&p_end=2&w_count_min=1&w_count_max=5

The application is backedup by a H2 database instance. to access it (in case you want to check the data), will have to go to:
http://localhost:8088/betvictor/h2-console/

on the login form, use as JDBC URL the next:
jdbc:h2:~/h2/epgjs/test;AUTO_SERVER=TRUE;MODE=MySQL

username: sa
password:
