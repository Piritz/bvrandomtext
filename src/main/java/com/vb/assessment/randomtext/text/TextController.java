package com.vb.assessment.randomtext.text;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is the controller that will handle the requests for the two different endpoints /text and /history.
 * As the assestment exercise says this is suppossed to be a REST service. In my opinion this would not be a REST service but a RPC one
 * since we are just exposing two endpoints without paying attention to follow the REST philosophy.
 * 
 *  REST services are based on resources and in my opinion and in the case we are handling the main resource would be the statistic object 
 *  which is the object that we return with the most used keyword and the averages. Following this, the /text endpoint should be a POST endpoint
 *  since we are generating a resource and returning it, meanwhile the /history endpoint is the one in charge of fetching the data as it is a GET endpoint.
 *  
 *  Said this I´m going to implement both endpoints in the same controller since they are handling the same resource but using a RPC aproximation.
 * 
 * 
 * @author piritz
 *
 */

@RestController
@RequestMapping(value = "")
public class TextController {
	
	static final Log logger = LogFactory.getLog(TextController.class);
	
	private TextService textDelegate;
	
	public TextController() {
	}
	
	@Inject
	public TextController(TextService textDelegate) {
		this.textDelegate = textDelegate;
	}
	
	
	/**
	 * This method calls to a service that will call the random service and will calculate the stats.
	 * 
	 * @param request
	 * @param pStart starting pharagraph number
	 * @param pEnd ending pharagraph number
	 * @param wordMinCount min number of words
	 * @param wordMaxCount max number of words
	 * @return
	 */
	@RequestMapping(value = "/text", method = RequestMethod.GET)
	public TextResponse calculateRandomTextStatistics(HttpServletRequest request, 
			@RequestParam(value = "p_start", defaultValue="1") int pStart,
			@RequestParam(value = "p_end", defaultValue="20") int pEnd,
			@RequestParam(value = "w_count_min", defaultValue="1") int wordMinCount, 
			@RequestParam(value = "w_count_max", defaultValue="5") int wordMaxCount) {
		logger.info("Starting calculateRandomTextStatistics");
		
		TextRequest textRequest = new TextRequest(pStart, pEnd, wordMinCount, wordMaxCount);
		
		TextStatisticsDTO statisticResults = textDelegate.calculateStatistics(textRequest);
		
		logger.info("Ending calculateRandomTextStatistics");
		return new TextResponse(statisticResults);
	}
	
	@RequestMapping(value = "/history", method = RequestMethod.GET)
	public List<TextResponse> fetchResultshistory(HttpServletRequest request) {
		logger.info("Starting fetchResultshistory");
		
		
		List<TextStatisticsDTO> statisticDTOList;
		try {
			statisticDTOList = textDelegate.fetchLastStatistics();
		} catch (Exception e) {
			throw new TextException("Error fetching history data");
		}
		List<TextResponse> result = new ArrayList<>();
		
		statisticDTOList.forEach(dto -> result.add(new TextResponse(dto)));
		
		logger.info("Ending fetchResultshistory");
		return result;
	}
	
}


