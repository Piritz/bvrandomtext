package com.vb.assessment.randomtext.text;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
@author: piritz
*/
@Repository
public interface TextHistoryRepository extends PagingAndSortingRepository<TextHistoryEntity, Long> {
}
