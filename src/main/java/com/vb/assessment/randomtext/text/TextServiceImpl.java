package com.vb.assessment.randomtext.text;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.client.WebTarget;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("textService")
public class TextServiceImpl implements TextService{
	
	static final Log logger = LogFactory.getLog(TextServiceImpl.class);
	
	private static final String NUMBER_PARAGRAPHS_PARAM = "p-{0}/";
	private static final String NUMBER_WORDS_PARAGRAPHS_PARAM = "{0}-{1}/";
	private static final String PARAGRAPH_REGEX_SPLITTER = "<p>(.*?)\\.<\\/p>\\r";

	private WebTarget randomTextTarget;
	private TextHistoryRepository historyRepository;
	private ObjectMapper objectMapper;
	
	public TextServiceImpl() {
	}
	
	@Inject
	public TextServiceImpl(WebTarget randomTextTarget,
			TextHistoryRepository historyRepository,
			ObjectMapper objectMapper) {
		this.randomTextTarget=randomTextTarget;
		this.historyRepository = historyRepository;
		this.objectMapper=objectMapper;
	}
	
	public TextStatisticsDTO calculateStatistics(TextRequest textRequest) {
		
		logger.info("Starting calculateStatistics. " + textRequest);
		
		TextStatisticsDTO callStatistics;
		TextStatisticsDTO result;
		ServiceResponseDTO response;
		Integer numberParagraphs=0;
		Long accumulatedTime=0L;
		Long startExecutionTime=0L;
		Long endExecutionTime=0L;
		List<String> usedWords=new ArrayList<>();
		
		checkInputParams(textRequest);
		
		startExecutionTime=Instant.now().toEpochMilli();
		for(int i=textRequest.getpStart();i<=textRequest.getpEnd();i++) {
			response=callService(i, textRequest.getWordMinCount(), textRequest.getWordMaxCount());
			callStatistics = computeResultIntoStastistic(response);
			//we update the total word list
			usedWords.addAll(callStatistics.getUsedWords());
			numberParagraphs += i;
			accumulatedTime += callStatistics.getAccumulatedProcessingTime();
		}
		
		endExecutionTime = Instant.now().toEpochMilli();
		logger.info("End calculateStatistics. " + textRequest);
		
		//we are going to prepare all the data to be returned
		//consider that total execution time is only time spent on calls and paragraphs processing time
		result=calculateFinalResults(numberParagraphs, accumulatedTime, endExecutionTime-startExecutionTime, usedWords);
		this.saveResultIntoDB(result);
		return result;
	}

	private TextStatisticsDTO calculateFinalResults(Integer numberParagraphs, Long accumulatedTime, Long totalTime, List<String> usedWords) {
		
		logger.info("Calculating final results. ");
		
		TextStatisticsDTO result=new TextStatisticsDTO();
		
		//We convert the total word list to a map counting every word appearance
		//After that we sort it and we get a linkedHashMap to get the order
		if(usedWords!=null) {// No words no averages
			Map<String, Long> valueCounts = usedWords.stream()
					.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
					.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, 
							(oldValue, newValue) -> oldValue, LinkedHashMap::new));
			
			logger.info("Computing ordered map" + valueCounts.toString());
			
			if(!valueCounts.isEmpty()) {
				result.setFreqWord(valueCounts.entrySet().iterator().next().getKey());
				//if Number of paragraphs is grater than 0 we process to calculate the average size of a paragraph. If not, will be setted to 0
				if(numberParagraphs >0) {
					result.setAvgParagraphSize((double)usedWords.size() / numberParagraphs);
					result.setAvgParagraphProcessingTime(accumulatedTime.doubleValue() / numberParagraphs);
				}
				
			}
		}
		
		result.setTotalProcessingTime(totalTime);
		
		logger.info("Final results calculated. " + result);
		
		return result;
	}

	private ServiceResponseDTO callService(int numPars, int wordMin, int wordMax) {
		
		try {
			return randomTextTarget.path(MessageFormat.format(NUMBER_PARAGRAPHS_PARAM, numPars))
					.path(MessageFormat.format(NUMBER_WORDS_PARAGRAPHS_PARAM, wordMin, wordMax))
					.request(MediaType.APPLICATION_JSON_VALUE).get(ServiceResponseDTO.class);
		}catch(WebApplicationException wae) {
			throw new TextException("Error in comunication with Random text service", wae);
		}catch(ResponseProcessingException rpe) {
			throw new TextException("Error procesing random text service response", rpe);
		}
		
	}
	
	private TextStatisticsDTO computeResultIntoStastistic(ServiceResponseDTO response) {
		logger.info("Starting computeResultIntoStastistic. " + response);
		
		Pattern pattern = Pattern.compile(PARAGRAPH_REGEX_SPLITTER);
		Matcher matcher = pattern.matcher(response.getTextOut());
		List<String> wordListTotal = new ArrayList<>();
		TextStatisticsDTO callStatistics = new TextStatisticsDTO();
		Long startingTime;
		Long endingTime;
		Long accumulatedTime=0L;
		
		//response will have n paragraphs and m words. we are going to count the paragraphs and set all the paragraphs words into a list
		//having the total count of words to get the average size of words per paragraph
		while(matcher.find()) {
			String extractedParagraph=matcher.group(1);
			//we are going to measure paragraph analyzing time. As the assestment doesn't mention it I will consider time in milliseconds
			startingTime = Instant.now().toEpochMilli();
			if(!StringUtils.isEmpty(extractedParagraph)) {
				List<String> wordList = Arrays.asList(extractedParagraph.split(" "));
				wordListTotal.addAll(wordList);
			}
			//we get ending paragraph analyzing time
			endingTime = Instant.now().toEpochMilli();
			
			//we accumulate the analyzing time to later calculate the average
			logger.info(MessageFormat.format("Calculating Pharagraph execution time with endingTime:{0} and startingTime:{1} = {2}", endingTime, startingTime, endingTime - startingTime));
			accumulatedTime += (endingTime - startingTime);
		}

		callStatistics.setUsedWords(wordListTotal);
		callStatistics.setAccumulatedProcessingTime(accumulatedTime);
		
		logger.info("Statistics obtained for response " + callStatistics);
		
		logger.info("Starting computeResultIntoStastistic. ");
		return callStatistics;
	}
	
	private void checkInputParams(TextRequest textRequest) throws TextException{
		logger.info("Starting checkInputParams " + textRequest);
		
		if(textRequest==null ) {
			throw new TextException("Request is null, impossible to get random text and its statistics");
		}
		if(textRequest.getpStart() < 1 ) {
			throw new TextException("PStart param must be greater or equal than one");
		}
		if(textRequest.getpEnd() < 1 ) {
			throw new TextException("PEnd param must be greater or equal than one");
		}
		if(textRequest.getWordMinCount() < 1 ) {
			throw new TextException("wordMinCount param must be greater or equal than one");
		}
		if(textRequest.getWordMaxCount() < 1 ) {
			throw new TextException("wordMaxCount param must be greater or equal than one");
		}
		if(textRequest.getpStart() > textRequest.getpEnd() ) {
			throw new TextException("PStart param must be lower or equal than pEnd param");
		}
		if(textRequest.getWordMinCount() > textRequest.getWordMaxCount() ) {
			throw new TextException("wordMinCount param must be lower or equal than wordMaxCount param");
		}
		
		logger.info("Ending checkInputParams " + textRequest);
	}

	private void saveResultIntoDB(TextStatisticsDTO dto) {
		TextHistoryEntity entity = new TextHistoryEntity();
		
		try {
			entity.setStatisticResults(objectMapper.writeValueAsString(dto));
			entity.setDateCreated(Date.from(LocalDateTime.now().atZone(ZoneId.of("UTC")).toInstant()));
			historyRepository.save(entity);
		} catch (JsonProcessingException e) {
			throw new TextException("Error saving statistics to db", e);
		}
	}
	
	@Override
	public List<TextStatisticsDTO> fetchLastStatistics() throws Exception {
		logger.info("Starting fetchLastStatistics ");
		Page<TextHistoryEntity> page;
		List<TextHistoryEntity> resultList;
		List<TextStatisticsDTO> result=new ArrayList<>();
		
		page = historyRepository.findAll(new PageRequest(0, 10, new Sort(Direction.DESC, "id")));
		resultList = page.getContent();
		
		if(resultList!=null && !resultList.isEmpty()) {
			resultList.forEach(entity->{
				try {
					result.add(objectMapper.readValue(entity.getStatisticResults(), TextStatisticsDTO.class));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		}
		
		logger.info("Ending checkInputParams with params" + result);
		return result;
	}
	
}
