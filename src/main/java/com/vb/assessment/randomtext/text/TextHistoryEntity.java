package com.vb.assessment.randomtext.text;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="bv_history")
public class TextHistoryEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name="id")
	private Long id;
	
	@Column(name="date_created")
	private Date dateCreated;
	
	@Lob
	@Column(name="statistic_results")
	private String statisticResults;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getStatisticResults() {
		return statisticResults;
	}

	public void setStatisticResults(String statisticResults) {
		this.statisticResults = statisticResults;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((statisticResults == null) ? 0 : statisticResults.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextHistoryEntity other = (TextHistoryEntity) obj;
		if (dateCreated == null) {
			if (other.dateCreated != null)
				return false;
		} else if (!dateCreated.equals(other.dateCreated))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (statisticResults == null) {
			if (other.statisticResults != null)
				return false;
		} else if (!statisticResults.equals(other.statisticResults))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TextHistoryEntity [id=" + id + ", dateCreated=" + dateCreated + ", statisticResults=" + statisticResults
				+ "]";
	}
	

}
