package com.vb.assessment.randomtext.text;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceResponseDTO {

	private String type;
	private Integer amount;
	private Integer number;
	@JsonProperty("number_max")
	private Integer numberMax;
	private String format;
	private String time;
	@JsonProperty("text_out")
	private String textOut;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public Integer getNumberMax() {
		return numberMax;
	}
	public void setNumberMax(Integer numberMax) {
		this.numberMax = numberMax;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getTextOut() {
		return textOut;
	}
	public void setTextOut(String textOut) {
		this.textOut = textOut;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((format == null) ? 0 : format.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((numberMax == null) ? 0 : numberMax.hashCode());
		result = prime * result + ((textOut == null) ? 0 : textOut.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceResponseDTO other = (ServiceResponseDTO) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (format == null) {
			if (other.format != null)
				return false;
		} else if (!format.equals(other.format))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (numberMax == null) {
			if (other.numberMax != null)
				return false;
		} else if (!numberMax.equals(other.numberMax))
			return false;
		if (textOut == null) {
			if (other.textOut != null)
				return false;
		} else if (!textOut.equals(other.textOut))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ServiceResponseDTO [type=" + type + ", amount=" + amount + ", number=" + number + ", numberMax="
				+ numberMax + ", format=" + format + ", time=" + time + ", textOut=" + textOut + "]";
	}
	
	
}
