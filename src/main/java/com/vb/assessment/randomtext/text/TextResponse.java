package com.vb.assessment.randomtext.text;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TextResponse {

	@JsonProperty("freq_word")
	private String freqWord;
	
	@JsonProperty("avg_paragraph_size")
	private Double avgParagraphSize;
	
	@JsonProperty("avg_paragraph_processing_time")
	private Double avgParagraphProcessingTime;
	
	@JsonProperty("total_processing_time")
	private Long totalProcessingTime;
	
	public TextResponse() {
		// TODO Auto-generated constructor stub
	}
	public TextResponse(TextStatisticsDTO statisticResults) {
		this.freqWord = statisticResults.getFreqWord();
		this.avgParagraphSize = statisticResults.getAvgParagraphSize();
		this.avgParagraphProcessingTime = statisticResults.getAvgParagraphProcessingTime();
		this.totalProcessingTime = statisticResults.getTotalProcessingTime();
	}

	public String getFreqWord() {
		return freqWord;
	}

	public void setFreqWord(String freqWord) {
		this.freqWord = freqWord;
	}

	public Double getAvgParagraphSize() {
		return avgParagraphSize;
	}

	public void setAvgParagraphSize(Double avgParagraphSize) {
		this.avgParagraphSize = avgParagraphSize;
	}

	public Double getAvgParagraphProcessingTime() {
		return avgParagraphProcessingTime;
	}

	public void setAvgParagraphProcessingTime(Double avgParagraphProcessingTime) {
		this.avgParagraphProcessingTime = avgParagraphProcessingTime;
	}

	public Long getTotalProcessingTime() {
		return totalProcessingTime;
	}

	public void setTotalProcessingTime(Long totalProcessingTime) {
		this.totalProcessingTime = totalProcessingTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((avgParagraphProcessingTime == null) ? 0 : avgParagraphProcessingTime.hashCode());
		result = prime * result + ((avgParagraphSize == null) ? 0 : avgParagraphSize.hashCode());
		result = prime * result + ((freqWord == null) ? 0 : freqWord.hashCode());
		result = prime * result + ((totalProcessingTime == null) ? 0 : totalProcessingTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextResponse other = (TextResponse) obj;
		if (avgParagraphProcessingTime == null) {
			if (other.avgParagraphProcessingTime != null)
				return false;
		} else if (!avgParagraphProcessingTime.equals(other.avgParagraphProcessingTime))
			return false;
		if (avgParagraphSize == null) {
			if (other.avgParagraphSize != null)
				return false;
		} else if (!avgParagraphSize.equals(other.avgParagraphSize))
			return false;
		if (freqWord == null) {
			if (other.freqWord != null)
				return false;
		} else if (!freqWord.equals(other.freqWord))
			return false;
		if (totalProcessingTime == null) {
			if (other.totalProcessingTime != null)
				return false;
		} else if (!totalProcessingTime.equals(other.totalProcessingTime))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TextStatistics [freqWord=" + freqWord + ", avgParagraphSize=" + avgParagraphSize
				+ ", avgParagraphProcessingTime=" + avgParagraphProcessingTime + ", totalProcessingTime="
				+ totalProcessingTime + "]";
	}
	
}
