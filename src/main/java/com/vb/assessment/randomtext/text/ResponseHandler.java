package com.vb.assessment.randomtext.text;

import java.io.IOException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ResponseHandler {

	public static <T> T getValue(ObjectMapper mapper, String json, Class<T> clazz ){
		try {
			return  mapper.readValue(json, clazz);
		} catch (IOException e) {
			try {
				ErrorBean errorBean = mapper.readValue(json, ErrorBean.class);
				throw new TextException("Error in service: " + errorBean.getErrorMessage(), errorBean);
			} catch (IOException jps) {
				throw new TextException("Unable to parse service response: " + json, jps);
			}
		}
	}
	
	public static <T> T getValue(ObjectMapper mapper, String json, TypeReference<T> typeReference){

		try {
			
			return mapper.readValue(json, typeReference);
			
		} catch (IOException e) {
			try {
				ErrorBean errorBean = mapper.readValue(json, ErrorBean.class);
				throw new TextException("Error in service: " + errorBean.getErrorMessage(), errorBean);
			} catch (IOException jps) {
				throw new TextException("Unable to parse service response: " + json, jps);
			}
		}
	}
	
	public static void checkVoid(ObjectMapper mapper, String json){
		try {
			if (!"".equals(json)) {
				ErrorBean errorBean = mapper.readValue(json, ErrorBean.class);
				throw new TextException("Error in service: " + errorBean.getErrorMessage(), errorBean);
			}
		} catch (IOException e) {
			throw new TextException("Unable to parse service response: " + json, e);
		}
	}
}
