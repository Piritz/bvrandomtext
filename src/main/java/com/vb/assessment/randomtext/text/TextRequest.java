package com.vb.assessment.randomtext.text;

public class TextRequest {
	private int pStart;
	private int pEnd;
	private int wordMinCount; 
	private int wordMaxCount;
	
	public TextRequest() {
		
	}
	
	public TextRequest(int pStart, int pEnd, int wordMinCount, int wordMaxCount) {
		this.pStart=pStart;
		this.pEnd=pEnd;
		this.wordMinCount=wordMinCount;
		this.wordMaxCount=wordMaxCount;
	}
	
	public int getpStart() {
		return pStart;
	}
	public void setpStart(int pStart) {
		this.pStart = pStart;
	}
	public int getpEnd() {
		return pEnd;
	}
	public void setpEnd(int pEnd) {
		this.pEnd = pEnd;
	}
	public int getWordMinCount() {
		return wordMinCount;
	}
	public void setWordMinCount(int wordMinCount) {
		this.wordMinCount = wordMinCount;
	}
	public int getWordMaxCount() {
		return wordMaxCount;
	}
	public void setWordMaxCount(int wordMaxCount) {
		this.wordMaxCount = wordMaxCount;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + pEnd;
		result = prime * result + pStart;
		result = prime * result + wordMaxCount;
		result = prime * result + wordMinCount;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextRequest other = (TextRequest) obj;
		if (pEnd != other.pEnd)
			return false;
		if (pStart != other.pStart)
			return false;
		if (wordMaxCount != other.wordMaxCount)
			return false;
		if (wordMinCount != other.wordMinCount)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "TextRequest [pStart=" + pStart + ", pEnd=" + pEnd + ", wordMinCount=" + wordMinCount + ", wordMaxCount="
				+ wordMaxCount + "]";
	}
}
