package com.vb.assessment.randomtext.text;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * This class is a copy of TextResponse plus some other attributes. We could have used this to return the results and manage the statistics
 * But want to separate the objects travelling to the browser (TextResponse) and the one used internally (this one)
 * @author piritz
 *
 */
public class TextStatisticsDTO {

	private String freqWord;
	
	private Double avgParagraphSize;
	
	private Double avgParagraphProcessingTime;
	
	private Long totalProcessingTime;
	
	private Long accumulatedProcessingTime;
	
	private List<String> usedWords;
	
	public TextStatisticsDTO() {
		this.freqWord = "";
		this.avgParagraphSize=0.0D;
		this.avgParagraphProcessingTime=0.0D;
		this.totalProcessingTime=0L;
		
	}
	
	public String getFreqWord() {
		return freqWord;
	}

	public void setFreqWord(String freqWord) {
		this.freqWord = freqWord;
	}

	public Double getAvgParagraphSize() {
		return avgParagraphSize;
	}

	public void setAvgParagraphSize(Double avgParagraphSize) {
		this.avgParagraphSize = avgParagraphSize;
	}

	public Double getAvgParagraphProcessingTime() {
		return avgParagraphProcessingTime;
	}

	public void setAvgParagraphProcessingTime(Double avgParagraphProcessingTime) {
		this.avgParagraphProcessingTime = avgParagraphProcessingTime;
	}

	public Long getTotalProcessingTime() {
		return totalProcessingTime;
	}

	public void setTotalProcessingTime(Long totalProcessingTime) {
		this.totalProcessingTime = totalProcessingTime;
	}

	public List<String> getUsedWords() {
		if(this.usedWords==null) {
			this.usedWords = new ArrayList<>();
		}
		return usedWords;
	}

	public void setUsedWords(List<String> usedWords) {
		this.usedWords = usedWords;
	}

	public Long getAccumulatedProcessingTime() {
		return accumulatedProcessingTime;
	}

	public void setAccumulatedProcessingTime(Long accumulatedProcessingTime) {
		this.accumulatedProcessingTime = accumulatedProcessingTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accumulatedProcessingTime == null) ? 0 : accumulatedProcessingTime.hashCode());
		result = prime * result + ((avgParagraphProcessingTime == null) ? 0 : avgParagraphProcessingTime.hashCode());
		result = prime * result + ((avgParagraphSize == null) ? 0 : avgParagraphSize.hashCode());
		result = prime * result + ((freqWord == null) ? 0 : freqWord.hashCode());
		result = prime * result + ((totalProcessingTime == null) ? 0 : totalProcessingTime.hashCode());
		result = prime * result + ((usedWords == null) ? 0 : usedWords.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextStatisticsDTO other = (TextStatisticsDTO) obj;
		if (accumulatedProcessingTime == null) {
			if (other.accumulatedProcessingTime != null)
				return false;
		} else if (!accumulatedProcessingTime.equals(other.accumulatedProcessingTime))
			return false;
		if (avgParagraphProcessingTime == null) {
			if (other.avgParagraphProcessingTime != null)
				return false;
		} else if (!avgParagraphProcessingTime.equals(other.avgParagraphProcessingTime))
			return false;
		if (avgParagraphSize == null) {
			if (other.avgParagraphSize != null)
				return false;
		} else if (!avgParagraphSize.equals(other.avgParagraphSize))
			return false;
		if (freqWord == null) {
			if (other.freqWord != null)
				return false;
		} else if (!freqWord.equals(other.freqWord))
			return false;
		if (totalProcessingTime == null) {
			if (other.totalProcessingTime != null)
				return false;
		} else if (!totalProcessingTime.equals(other.totalProcessingTime))
			return false;
		if (usedWords == null) {
			if (other.usedWords != null)
				return false;
		} else if (!usedWords.equals(other.usedWords))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TextStatisticsDTO [freqWord=" + freqWord + ", avgParagraphSize=" + avgParagraphSize
				+ ", avgParagraphProcessingTime=" + avgParagraphProcessingTime + ", totalProcessingTime="
				+ totalProcessingTime + ", accumulatedProcessingTime=" + accumulatedProcessingTime + ", usedWords="
				+ usedWords + "]";
	}

}
