package com.vb.assessment.randomtext.text;

/**
 * This class represents a generic ErrorBean, it contains information about it like the error message. The error flag is
 * meant to provide easy identification of error when handled as JSON
 * 
 *
 */
public class ErrorBean {
    /**
     * The error flag is meant to provide easy identification of error when handled as JSON
     */
    private final boolean error;
    private String errorMessage;
    private int errorCode;

    public ErrorBean(){
        this.error = true;
    }
    
    public ErrorBean(String errorMessage, int errorCode) {
        this.error = true;
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }

    public ErrorBean(TextException e) {
        this.error = true;
        this.errorMessage = e.getMessage();
        this.errorCode = e.getErrorCode();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isError() {
        return error;
    }

}
