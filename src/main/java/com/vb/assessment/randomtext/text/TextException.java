package com.vb.assessment.randomtext.text;

public class TextException extends RuntimeException {

    private static final long serialVersionUID = 2322157886464472235L;

    private final int errorCode;
    private final ErrorBean errorBean;

    public TextException(String message) {
        super(message);
        errorCode = -1;
        errorBean = null;
    }

    public TextException(String message, Throwable cause) {
        super(message, cause);
        errorCode = -1;
        errorBean = null;
    }

    public TextException(String message, ErrorBean errorBean) {
        super(message);
        errorCode = -1;
        this.errorBean = errorBean;
    }

    public TextException(String message, Throwable cause, ErrorBean errorBean) {
        super(message, cause);
        errorCode = -1;
        this.errorBean = errorBean;
    }

    public TextException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
        errorBean = null;
    }

    public TextException(String message, Throwable cause, int errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
        errorBean = null;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public ErrorBean getErrorBean() {
        return errorBean;
    }

}
