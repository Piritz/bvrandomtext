package com.vb.assessment.randomtext.text;

import java.util.List;

public interface TextService {
	public TextStatisticsDTO calculateStatistics(TextRequest textRequest) throws TextException;
	public List<TextStatisticsDTO> fetchLastStatistics()throws Exception;
}
