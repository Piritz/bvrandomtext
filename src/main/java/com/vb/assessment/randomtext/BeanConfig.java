package com.vb.assessment.randomtext;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class BeanConfig {

	@Value("${betvictor.services.randomTextUrl}")
	private String serviceUrl;
	
	@Bean
	public Client httpClient() {
		return ClientBuilder.newClient();
	}
	
	@Bean
	public WebTarget randomTextTarget(Client httpClient) {
		return httpClient.target(serviceUrl);
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}
}
