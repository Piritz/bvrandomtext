/**
 * 
 */
package com.vb.assessment.randomtext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.vb.assessment.randomtext.text.ErrorBean;
import com.vb.assessment.randomtext.text.TextException;

/**
 * @author piritz
 *
 */
@ControllerAdvice
public class TextServiceControllerAdvice {
	private static Logger logger = LoggerFactory.getLogger(TextServiceControllerAdvice.class);

	@ExceptionHandler
	@ResponseBody
	public Object handleEPGPayException(TextException e, HttpServletRequest request, HttpServletResponse response) {
		logger.error(e.getMessage(), e);
		response.setStatus(500);
		return new ErrorBean(e);
	}
	
	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public Object handleAllExceptions(Exception e, HttpServletRequest request, HttpServletResponse response) {
		logger.error(e.getMessage(), e);
		response.setStatus(500);
		return new ErrorBean(e.getMessage(), -1);
	}
	
}
