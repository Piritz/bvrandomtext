package com.vb.assessment.randomtext.text;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@EnableAutoConfiguration
@SpringBootTest
public class TextControllerTest {

	
	private static final String FREQ_WORD ="skina";
	private static final String FREQ_WORD2 ="skina2";
	private static final String PARAGRAPH_START_VAR ="p_start";
	private static final String PARAGRAPH_END_VAR ="p_end";
	private static final String WORD_COUNT_MIN ="w_count_min";
	private static final String WORD_COUNT_MAX ="w_count_max";
	private static final String EXPECTED_OK_RESULT ="{\"freq_word\":\"skina\",\"avg_paragraph_size\":2.0,\"avg_paragraph_processing_time\":5.0,\"total_processing_time\":5}";
	private static final String EXPECTED_OK_RESULT_LIST ="[{\"freq_word\":\"skina\",\"avg_paragraph_size\":2.0,\"avg_paragraph_processing_time\":5.0,\"total_processing_time\":5},{\"freq_word\":\"skina2\",\"avg_paragraph_size\":6.0,\"avg_paragraph_processing_time\":666.0,\"total_processing_time\":666}]";
	private static final String ERROR_EXCEPTION_MESSAGE = "Exception test message";
	private static final int INITIAL_PARAGRAPH = 1;
	private static final int FINAL_PARAGRAPH = 10;
	private static final int WORD_MIN = 1;
	private static final int WORD_MAX = 5;
	
	private TextStatisticsDTO statisticsResult;
	private TextStatisticsDTO historyDto;
	private TextRequest textRequest;
	private List<TextStatisticsDTO> historyResult;
	
	@Inject
	private MockMvc mockMvc;
	
	@MockBean
	private TextService textService;
	
	@Before
	public void setUp() {
		statisticsResult = new TextStatisticsDTO();
		
		statisticsResult.setFreqWord(FREQ_WORD);
		statisticsResult.setAvgParagraphSize(2.0D);
		statisticsResult.setAvgParagraphProcessingTime(5.0D);
		statisticsResult.setTotalProcessingTime(5L);
		
		textRequest = new TextRequest(INITIAL_PARAGRAPH, FINAL_PARAGRAPH, WORD_MIN, WORD_MAX);
		
		historyResult = new ArrayList<>();
		historyResult.add(statisticsResult);
		historyDto = new TextStatisticsDTO();
		historyDto.setFreqWord(FREQ_WORD2);
		historyDto.setAvgParagraphSize(6.0D);
		historyDto.setAvgParagraphProcessingTime(666.0D);
		historyDto.setTotalProcessingTime(666L);
		historyResult.add(historyDto);
	}
	
	@Test
	public void shouldReturnStatisticsForWords() throws Exception{
		MvcResult result;
		
		
		given(textService.calculateStatistics(textRequest))
						.willReturn(statisticsResult);
		
		result = mockMvc.perform(
				get("/text/")
				.param(PARAGRAPH_START_VAR, String.valueOf(INITIAL_PARAGRAPH))
				.param(PARAGRAPH_END_VAR, String.valueOf(FINAL_PARAGRAPH))
				.param(WORD_COUNT_MIN, String.valueOf(WORD_MIN))
				.param(WORD_COUNT_MAX, String.valueOf(WORD_MAX))
				)
				.andExpect(status().isOk())
				.andReturn();
		
		assertThat(result).isNotNull();
		assertThat(result.getResponse()).isNotNull();
		assertThat(result.getResponse().getContentAsString()).isNotNull();
		assertThat(result.getResponse().getContentAsString()).isEqualTo(EXPECTED_OK_RESULT);
		
	}

	@Test
	public void shouldRespondWithInternalServerErrorGettingStatistics() throws Exception{
		
		given(textService.calculateStatistics(textRequest))
			.willThrow(new TextException(ERROR_EXCEPTION_MESSAGE));
		
		mockMvc.perform(
				get("/text/")
				.param(PARAGRAPH_START_VAR, String.valueOf(INITIAL_PARAGRAPH))
				.param(PARAGRAPH_END_VAR, String.valueOf(FINAL_PARAGRAPH))
				.param(WORD_COUNT_MIN, String.valueOf(WORD_MIN))
				.param(WORD_COUNT_MAX, String.valueOf(WORD_MAX))
				)
				.andExpect(status().is5xxServerError());
		
	}
	
	@Test
	public void shouldReturnHistoricResults() throws Exception{
		MvcResult result;
		
		
		given(textService.fetchLastStatistics())
						.willReturn(historyResult);
		
		result = mockMvc.perform(
				get("/history"))
				.andExpect(status().isOk())
				.andReturn();
		
		assertThat(result).isNotNull();
		assertThat(result.getResponse()).isNotNull();
		assertThat(result.getResponse().getContentAsString()).isNotNull();
		assertThat(result.getResponse().getContentAsString()).isEqualTo(EXPECTED_OK_RESULT_LIST);
		
	}
	
	@Test
	public void shouldFailFetchingHistory() throws Exception{
		
		given(textService.fetchLastStatistics())
						.willThrow(new TextException(ERROR_EXCEPTION_MESSAGE));
		
		mockMvc.perform(
				get("/history"))
				.andExpect(status().is5xxServerError());
		
		
	}
	
}
