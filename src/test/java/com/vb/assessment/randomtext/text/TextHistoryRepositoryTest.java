package com.vb.assessment.randomtext.text;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Calendar;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest
public class TextHistoryRepositoryTest {

	private static final String JSON_DATA_2_STORE = "{\"freqWord\":\"far\",\"avgParagraphSize\":2.6545454545454548,\"avgParagraphProcessingTime\":0.01818181818181818,\"totalProcessingTime\":620,\"accumulatedProcessingTime\":null,\"usedWords\":[]}";
	private static final Long RECORD_TO_FETCH = 66L;
	
	private TextHistoryEntity entity;
	
	@Inject
	private TextHistoryRepository repo;
	
	@Before
	public void setUp() {
		entity = new TextHistoryEntity();
		entity.setId(1L);
		entity.setStatisticResults(JSON_DATA_2_STORE);
		entity.setDateCreated(Calendar.getInstance().getTime());
	}
	
	
	@Test
	@Transactional
	public void shouldSaveEntityAndRetrieveItFromDB() {
		TextHistoryEntity tmp = new TextHistoryEntity();
		repo.save(entity);
		tmp = repo.findOne(1L);
		assertThat(tmp).isEqualTo(entity);
	}
	
	@Test
	@Transactional
	public void shouldRetrieveItFromDBRecordFromDataFile() {
		TextHistoryEntity tmp = new TextHistoryEntity();
		
		tmp = repo.findOne(RECORD_TO_FETCH);
		assertThat(tmp).isNotNull();
		assertThat(tmp.getStatisticResults()).isNotNull().isNotEmpty().isEqualTo(JSON_DATA_2_STORE);
	}

}
