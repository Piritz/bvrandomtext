package com.vb.assessment.randomtext.text;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockito.BDDMockito.given;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.junit.MockServerRule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class TextServiceImplTest {

	private static final String SERVICE_ERROR_PARSING ="Error procesing random text service response";
	private static final String SERVICE_ERROR_COMM ="Error in comunication with Random text service";
	private static final String ERROR_PSTART_LESS_1 ="PStart param must be greater or equal than one";
	private static final String ERROR_PEND_LESS_1 ="PEnd param must be greater or equal than one";
	private static final String ERROR_MIN_COUNT_LESS_1 ="wordMinCount param must be greater or equal than one";
	private static final String ERROR_MAX_COUNT_LESS_1 ="wordMaxCount param must be greater or equal than one";
	private static final String ERROR_PSTART_BIGGER_PEND ="PStart param must be lower or equal than pEnd param";
	private static final String ERROR_MINCOUNT_BIGGER_MAXCOUNT ="wordMinCount param must be lower or equal than wordMaxCount param";
	private static final String ERROR_NULL_REQUEST ="Request is null, impossible to get random text and its statistics";
	private static final String RANDOM_URL ="http://localhost:{0}/api/giberish/";
	private static final String RANDOM_PATH = "/api/giberish/p-1/1-5/";
	private static final String RANDOM_RESPONSE = "{\"type\":\"gibberish\",\"amount\":1,\"number\":\"1\",\"number_max\":\"5\",\"format\":\"p\",\"time\":\"00:24:31\",\"text_out\":\"<p>Opaque wherever opaque.<\\/p>\\r\"}";
	private static final String RANDOM_RESPONSE_NO_WORD = "{\"type\":\"gibberish\",\"amount\":1,\"number\":\"1\",\"number_max\":\"5\",\"format\":\"p\",\"time\":\"00:24:31\",\"text_out\":\"<p>.<\\/p>\\r\"}";
	private static final String MALFORMED_RANDOM_RESPONSE = "{\"notype\":\"gibberish\",\"amont\":1,\"nomber\":\"1\",\"number_max\":\"5\",\"format\":\"p\",\"time\":\"00:24:31\",\"text_out\":\"<p>Opaque wherever opaque.<\\/p>\\r\"}";
	private static final String REPEATED_WORD = "opaque";
	private static final String JSON_STATISTIC = "{\"freqWord\":\"far\",\"avgParagraphSize\":2.6545454545454548,\"avgParagraphProcessingTime\":0.01818181818181818,\"totalProcessingTime\":620,\"accumulatedProcessingTime\":null,\"usedWords\":[]}";
	private static final int INITIAL_PARAGRAPH = 1;
	private static final int FINAL_PARAGRAPH = 1;
	private static final int FINAL_PARAGRAPH_3 = 3;
	private static final int WORD_MIN = 1;
	private static final int WORD_MAX = 5;
	private static final int RANDOM_RESPONSE_PHARAGRAPH_WORDS = 3;
	private static final int RANDOM_RESPONSE_PHARAGRAPH_1 = 1;
	
	private TextRequest textRequest;
	private Client client;
	private WebTarget randomTextTarget;
	private MockServerClient mockServerClient;
	private TextHistoryEntity textHistoryEntity;
	
	private TextServiceImpl serviceImplTest;
	
	@Mock
	private ObjectMapper objectMapper;
	
	@Mock
	private TextHistoryRepository historyRepository;
	
	@Rule
	public MockServerRule mockServerRule = new MockServerRule(this);
	
	@Before
	public void setUp() {
		client = ClientBuilder.newClient();
		randomTextTarget = client.target(MessageFormat.format(RANDOM_URL, String.valueOf(mockServerRule.getPort())));
		serviceImplTest = new TextServiceImpl(randomTextTarget, historyRepository, objectMapper);
		serviceImplTest = Mockito.spy(serviceImplTest);
		
		textRequest = new TextRequest(INITIAL_PARAGRAPH, FINAL_PARAGRAPH, WORD_MIN, WORD_MAX);
		
		textHistoryEntity = new TextHistoryEntity();
		textHistoryEntity.setId(66L);
		textHistoryEntity.setDateCreated(Calendar.getInstance().getTime());
		textHistoryEntity.setStatisticResults(JSON_STATISTIC);
	}
	
	@Test
	public void shouldCallServiceAndReturnStatistics() throws Exception{
		
		mockServerClient.when(request(RANDOM_PATH))
		.respond(response().withStatusCode(200)
				.withHeader("Content-Type", "application/json;charset=UTF-8")
			.withBody(RANDOM_RESPONSE));
		
		TextStatisticsDTO result = serviceImplTest.calculateStatistics(textRequest);
		
		assertThat(result).isNotNull();
		assertThat(result.getFreqWord()).isEqualToIgnoringCase(REPEATED_WORD);
		assertThat(result.getAvgParagraphSize()).isEqualTo(RANDOM_RESPONSE_PHARAGRAPH_WORDS / RANDOM_RESPONSE_PHARAGRAPH_1);
		
	}
	
	@Test
	public void shoulThrowExceptionCommunicatingWithRandomService() throws Exception{
		
		mockServerClient.when(request(RANDOM_PATH))
		.respond(response().withStatusCode(500)
				.withHeader("Content-Type", "application/json;charset=UTF-8")
			.withBody(RANDOM_RESPONSE));
		try {
			serviceImplTest.calculateStatistics(textRequest);
		}catch(TextException te) {
			assertThat(te.getMessage()).isEqualTo(SERVICE_ERROR_COMM);
		}
		
		
	}
	
	@Test
	public void shoulThrowExceptionParsingServiceResponse() throws Exception{
		
		mockServerClient.when(request(RANDOM_PATH))
		.respond(response().withStatusCode(200)
				.withHeader("Content-Type", "application/json;charset=UTF-8")
			.withBody(MALFORMED_RANDOM_RESPONSE));
		try {
			serviceImplTest.calculateStatistics(textRequest);
		}catch(TextException te) {
			assertThat(te.getMessage()).isEqualTo(SERVICE_ERROR_PARSING);
		}
		
		
	}
	
	@Test
	public void shouldCallServiceAndReturnStatisticsEmptyFrequentWord() throws Exception{
		
		mockServerClient.when(request(RANDOM_PATH))
		.respond(response().withStatusCode(200)
				.withHeader("Content-Type", "application/json;charset=UTF-8")
			.withBody(RANDOM_RESPONSE_NO_WORD));
		
		TextStatisticsDTO result = serviceImplTest.calculateStatistics(textRequest);
		
		assertThat(result).isNotNull();
		assertThat(result.getFreqWord()).isEmpty();;
		assertThat(result.getAvgParagraphSize()).isEqualTo(0);
		assertThat(result.getAvgParagraphProcessingTime()).isEqualTo(0);
		
	}
	
	@Test
	public void shouldRespondWithExceptionWhenInitalParagraphIsLowerThanOne() throws Exception{
		
		textRequest.setpStart(0);
		
		try {
			serviceImplTest.calculateStatistics(textRequest);
		}catch(TextException te) {
			assertThat(te.getMessage()).isNotNull().isEqualTo(ERROR_PSTART_LESS_1);
		}
		
	}
	
	@Test
	public void shouldRespondWithExceptionWhenENDParagraphIsLowerThanOne() throws Exception{
		
		textRequest.setpEnd(0);
		
		try {
			serviceImplTest.calculateStatistics(textRequest);
		}catch(TextException te) {
			assertThat(te.getMessage()).isNotNull().isEqualTo(ERROR_PEND_LESS_1);
		}
		
	}
	
	@Test
	public void shouldRespondWithExceptionWhenMinWordCountIsLowerThanOne() throws Exception{
		
		textRequest.setWordMinCount(0);
		
		try {
			serviceImplTest.calculateStatistics(textRequest);
		}catch(TextException te) {
			assertThat(te.getMessage()).isNotNull().isEqualTo(ERROR_MIN_COUNT_LESS_1);
		}
		
	}
	
	@Test
	public void shouldRespondWithExceptionWhenMaxWordCountIsLowerThanOne() throws Exception{
		
		textRequest.setWordMaxCount(0);
		
		try {
			serviceImplTest.calculateStatistics(textRequest);
		}catch(TextException te) {
			assertThat(te.getMessage()).isNotNull().isEqualTo(ERROR_MAX_COUNT_LESS_1);
		}
		
	}
	
	@Test
	public void shouldRespondWithExceptionWhenParagraphInitIsBigerThanEnd() throws Exception{
		
		textRequest.setpStart(FINAL_PARAGRAPH_3);
		textRequest.setpEnd(INITIAL_PARAGRAPH);
		
		try {
			serviceImplTest.calculateStatistics(textRequest);
		}catch(TextException te) {
			assertThat(te.getMessage()).isNotNull().isEqualTo(ERROR_PSTART_BIGGER_PEND);
		}
		
	}
	
	@Test
	public void shouldRespondWithExceptionWhenWordMinCountIsBiggerThanWordMaxCount() throws Exception{
		
		textRequest.setWordMinCount(WORD_MAX);
		textRequest.setWordMaxCount(WORD_MIN);
		
		try {
			serviceImplTest.calculateStatistics(textRequest);
		}catch(TextException te) {
			assertThat(te.getMessage()).isNotNull().isEqualTo(ERROR_MINCOUNT_BIGGER_MAXCOUNT);
		}
		
	}
	
	@Test
	public void shouldRespondWithExceptionWhenRequestNull() throws Exception{
		
		textRequest.setWordMinCount(WORD_MAX);
		textRequest.setWordMaxCount(WORD_MIN);
		
		try {
			serviceImplTest.calculateStatistics(null);
		}catch(TextException te) {
			assertThat(te.getMessage()).isNotNull().isEqualTo(ERROR_NULL_REQUEST);
		}
		
	}

	@Test
	public void shouldFetchStatistics() throws Exception{
		Page<TextHistoryEntity> pageStatistics;
		List<TextHistoryEntity>listStatistics;
		List<TextStatisticsDTO> result;
		
		listStatistics = new ArrayList<>();
		listStatistics.add((TextHistoryEntity)textHistoryEntity);
		pageStatistics = new PageImpl<>(listStatistics, new PageRequest(1,1), listStatistics.size());
		
		given(historyRepository.findAll(Mockito.any(PageRequest.class))).willReturn(pageStatistics);
		result = serviceImplTest.fetchLastStatistics();
		
		TextStatisticsDTO dto = objectMapper.readValue(JSON_STATISTIC, TextStatisticsDTO.class);
		assertThat(result).isNotNull().isNotEmpty();
		assertThat(result.get(0)).isEqualTo(dto);
		
	}
	
	@Test
	public void shouldFetchStatisticsAndReturnEmptyList() throws Exception{
		Page<TextHistoryEntity> pageStatistics;
		List<TextHistoryEntity>listStatistics;
		List<TextStatisticsDTO> result;
		
		listStatistics = new ArrayList<>();
		pageStatistics = new PageImpl<>(listStatistics, new PageRequest(1,1), listStatistics.size());
		
		given(historyRepository.findAll(Mockito.any(PageRequest.class))).willReturn(pageStatistics);
		result = serviceImplTest.fetchLastStatistics();
		
		assertThat(result).isNotNull().isEmpty();;
		
	}
	
	@Test
	public void shouldFetchStatisticsAndReturnNullList() throws Exception{
		Page<TextHistoryEntity> pageStatistics;
		List<TextStatisticsDTO> result;
		
		
		pageStatistics = Mockito.mock(Page.class);
		
		given(historyRepository.findAll(Mockito.any(PageRequest.class))).willReturn(pageStatistics);
		given(pageStatistics.getContent()).willReturn(null);
		result = serviceImplTest.fetchLastStatistics();
		
		assertThat(result).isNotNull().isEmpty();;
		
	}
	
}
